#!/bin/sh
# Ports Build Bot
CWD=$(pwd)
BUILDS=${BUILDS:-games/flashplayer games/tl-legacy lang/scratch graphics/pencil2d-linux ports-mgmt/port www/seamonkey games/polymc x11/xclicker x11/winr x11/jsm}
PKGEXT=pkg # set it to txz if you're on older FreeBSD than 13.0
REPO_PATH=${REPO_PATH:-$CWD/../delports/repo}
BOT_MAKE_FLAGS="-DBATCH" # don't configure ports

for flag in $*
do
	case "$flag" in
		-b)
			export BUILD=TRUE
			;;
		-r)
			export REFRESH_REPO=TRUE
			;;
		-d)
			export DELETE_OLD_REPO_PKGS=TRUE
			;;
		*)
			export HELP=TRUE
			;;
	esac
done

echo "Starting Build Bot with args $*"

if [ "$HELP" == "TRUE" ]
then
	echo "Basic Build Bot version infinity"
	echo "Usage: $0 [options]"
	echo "Or: REPO_PATH=/path/to/repo/dir BUILDS=\"CATEGORY/PORT CATEGORY/PORT...\" $0 [options]"
	echo "Options:"
	echo "-b          Build all ports specified in BUILDS variable"
	echo "-r          Update repository specified in REPO_PATH variable"
	echo "-d          Delete old packages in REPO_PATH"
	echo
	echo "Example: '$0 -d -b -r' means '$0 clean repo, build packages and add it to repo; update repository with current entries'"
	exit 0
fi

if [ "$DELETE_OLD_REPO_PKGS" == "TRUE" ]
then
	echo "==> Removing old repo files"
	rm -rvf $REPO_PATH/*-*.${PKGEXT}
fi
if [ "$BUILD" == "TRUE" ]
then
	echo "==> Building ports $BUILDS"
	cd $CWD
	for bot_port in $BUILDS
	do
		cd $CWD
		if [ -f "$bot_port/Makefile" ]
		then
			cd $CWD
			echo "=> Building $bot_port"
			cd $bot_port
			make $BOT_MAKE_FLAGS package
			echo "=> Copying package to $REPO_PATH"
			mkdir -p $REPO_PATH
			cp -v work/pkg/* $REPO_PATH/
			make clean
		fi
		if [ ! -f "$bot_port/Makefile" ]
		then
			echo "!> Port $bot_port not found; skipping"
		fi
	done
fi
if [ "$REFRESH_REPO" == "TRUE" ]
then
	cd $REPO_PATH
	echo "==> Updating repository $REPO_PATH"
	pkg repo .
	echo "==> done"
fi
echo "Bot tasks completed. Exiting"
exit 0
