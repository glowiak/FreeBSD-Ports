import javax.swing.JOptionPane;

public class Notifier
{
    public static void main(String[] args)
    {
        JOptionPane.showMessageDialog(null, "Thank you for installing PolyMC! Please note that Minecraft officialy\n does not support the FreeBSD operating system. For your comfort, we have managed to get it running,\n but you need to do something too. If you want to run Minecraft 1.12.2 \nor lower, change te Java executable path to /usr/local/share/minecraft-client/minecraft-runtime,\nwhile if you want to run Minecraft 1.13 or higher you need to use\n/usr/local/share/minecraft-client/lwjgl3-runtime as the executable file.\nOf course assuming that you have the minecraft-client and lwjgl3 packages installed.\nThank you for your support!");
    }
}
