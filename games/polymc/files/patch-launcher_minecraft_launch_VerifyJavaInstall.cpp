--- launcher/minecraft/launch/VerifyJavaInstall.cpp.orig	2022-06-01 08:39:32.260313000 +0200
+++ launcher/minecraft/launch/VerifyJavaInstall.cpp	2022-05-30 07:50:15.446723000 +0200
@@ -49,24 +49,16 @@
     auto compatibleMajors = packProfile->getProfile()->getCompatibleJavaMajors();
 
     JavaVersion javaVersion(storedVersion);
+    emitSucceeded();
+    return;
 
-    if (compatibleMajors.isEmpty() || compatibleMajors.contains(javaVersion.major()))
-    {
-        emitSucceeded();
-        return;
-    }
 
-
     if (ignoreCompatibility)
     {
         emit logLine(tr("Java major version is incompatible. Things might break."), MessageLevel::Warning);
         emitSucceeded();
         return;
     }
-
-    emit logLine(tr("This instance is not compatible with Java version %1.\n"
-                    "Please switch to one of the following Java versions for this instance:").arg(javaVersion.major()),
-                 MessageLevel::Error);
     for (auto major : compatibleMajors)
     {
         emit logLine(tr("Java version %1").arg(major), MessageLevel::Error);
