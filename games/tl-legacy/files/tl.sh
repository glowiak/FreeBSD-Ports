#!/bin/sh

APP_NAME="TL"
JAVA_HOME=/usr/local/openjdk8
LDIR=/opt/tlauncher

if [ ! -f "${JAVA_HOME}/bin/java" ]
then
    echo "ERROR: You need Java 8 installed to run ${APP}!"
    exit 1
fi

${JAVA_HOME}/bin/java -jar ${LDIR}/launcher.jar
